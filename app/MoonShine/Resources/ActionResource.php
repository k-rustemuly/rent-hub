<?php

namespace App\MoonShine\Resources;

use Illuminate\Database\Eloquent\Model;
use App\Models\Action;

use MoonShine\Resources\Resource;
use MoonShine\Fields\ID;
use MoonShine\Actions\FiltersAction;
use MoonShine\Fields\Date;
use MoonShine\Fields\NoInput;

class ActionResource extends Resource
{
	public static string $model = Action::class;

	public static string $title = 'Actions';

    public static array $activeActions = ['show', 'delete'];

	public function fields(): array
	{
		return [
            ID::make()->sortable(),
            NoInput::make('name', 'name'),
            Date::make(
                trans('moonshine::ui.resource.created_at'),
                'created_at'
            )
                ->format("d.m.Y")
                ->sortable()
                ->hideOnForm()
        ];
	}

	public function rules(Model $item): array
	{
        return [];
    }

    public function search(): array
    {
        return ['id', 'name'];
    }

    public function filters(): array
    {
        return [];
    }

    public function actions(): array
    {
        return [
            FiltersAction::make(trans('moonshine::ui.filters')),
        ];
    }
}
